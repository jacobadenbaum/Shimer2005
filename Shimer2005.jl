#=
Name:   Shimer2005.jl
Author: Jacob Adenbaum
Date:   4/12/2018

This code will replicate Table 3 from Shimer's 2005 AER on the
cyclicality of unemployment and vacancies.
=#

using ForwardDiff, DataFrames, PyPlot, Parameters, Distributions

# These are unregistered packages that I have written -- available at
# git@gitlab.com:jacobadenbaum/[PackageName].jl
using Tables, HPFilter

########################################################################
#################### Model Primitives ##################################
########################################################################

f(m, θ) = m.μ*θ^(1-m.α)     # Job Finding Function
q(m, θ) = f(m,θ)/θ          # Vacancy Filling Function

@with_kw mutable struct Shimer2005{TF<:AbstractFloat, TI<:Integer}
    s::TF = 0.1             # Separation Rate
    r::TF = 0.012           # Discount Rate
    z::TF = 0.4             # Value of Leisure
    μ::TF = 1.355           # Scale of matching function
    α::TF = 0.72            # Matching Elasticity
    c::TF = 0.213           # Cost of vacancy c
    σ::TF = 0.0165          # Standard Deviation
    γ::TF = 0.004
    n::TI = 1000            # Number of Grid Points
    λ::TF = n*γ             # Poisson Arrival Rate
    Δ::TF = σ/sqrt(λ)       # Grid Size
    ygrid::Vector{TF} = linspace(-n*Δ, n*Δ, 2n + 1) |> collect
    pgrid::Vector{TF} = z .+ exp.(ygrid)*(1-z) 
    β::Vector{TF}     = α*ones(2n+1)   
    # Construct Transition Probabilities between states 
    X::Matrix{TF}     = begin
        Pup     = 1/2*(1 - ygrid./(n*Δ))[1:end-1]
        Pdown   = 1/2*(1 + ygrid./(n*Δ))[2:end]
        Tridiagonal(Pdown, zeros(2n+1), Pup) |> full
    end
    # Initial Guess for Θ
    θ::Vector{TF}     = ones(2n+1)
end

function ptable(m::Shimer2005, name::AbstractString)
    @unpack s, r, z, μ, α, c, σ, γ, n, λ, Δ = m 
    
    return TableCol(name, 
           Dict("Separation rate \$s\$"  =>s,
               "Discount rate \$r\$"    =>r,
               "Value of leisure \$z\$" =>z,
               "Scale of matching function \$\\mu\$"=>μ,
               "Matching Elasticity \$\\alpha\$"=>α,
               "Cost of vacancy \$c\$"  =>c,
               "Standard Deviation of shocks \$\\sigma\$"=>σ,
               "Autoregressive parameter \$\\gamma\$"=>γ,
               "Number of grid points \$2n +1\$"=>2n+1,
               "Grid Size \$\\Delta\$"  =>Δ))
end

########################################################################
############### Solve for u/v ratio (θ) state by state #################
########################################################################


"""
```
g(m::Shimer2005, θ)
```
The residual equation for the equilibrium condition that defines θ state
by state.  This function will be used in the root finding algorithm.  

θ must have dimension (m.n, 1) or else the code will throw an error.  
"""
function g(m::Shimer2005, θ)  
    @unpack r, s, λ, β, pgrid, z, c, X = m
    return (r+s+λ)./q.(m,θ) + β.*θ - (1.-β).*(pgrid.-z)/c-λ*X*(1./q.(m,θ))
end


"""
```
Dg(m::Shimer2005, θ)
```
For a given parameter vector θ, computes the jacobian of the residual
function for the equilibrium condition defining θ state by state.  This
function will be used in the root finding algorithm.

θ must have dimension (m.n, 1) or else the code will throw an error.  

NOTE: The jacobian is too large (2001 x 2001 in our standard
calibration) to compute efficiently with automatic differentiation.
Instead, we exploit its sparsity to construct it with minimal
calculations, using automatic differentiation only to we compute the
derivative of the job filling rate with respect to θ and then then
constructing the (mostly sparse) jacobian by hand.  
"""
Dg(m::Shimer2005, θ) = begin
    
    @unpack r, s, λ, β, X = m
    
    # Differentiate 1/q(θ)
    Dq = ForwardDiff.derivative.(x->1.0/q(m,x), θ)

    # Get the diagonal portion
    d = (r+s+λ)*Dq + β |> diagm

    # Get the tridiagonal portion
    t = λ*copy(X)
    scale!(t, Dq)

    return (d - t)
end


"""
```
solve!(m::Shimer2005; abs_tol=1e6)
```
Solves for the equilibrium θ state by state with a Newton root finding
algorithm.  Note that in order to stabilize the algorithm, we reflect
any negative values of θ back into the positive reals.  Since this
transformation is just the identity map when restricted to some open
neighborhood of the true solution, this does not change the asymptotic
properties of the algorithm. 
"""
function solve!(m::Shimer2005; abs_tol=1e-6)
    
    @unpack θ = m

    # Initialize the problem
    err = Inf
    gθ  = g(m,θ)

    while !(err<abs_tol)

        # Update to a new θ
        θ  = θ - Tridiagonal(Dg(m, θ))\gθ

        # Ugh -- sometimes we might send the θs below zero.  Domain
        # errors are no fun...
        θ  = abs.(θ)

        # Recompute the value of g at θ
        gθ  = g(m, θ)
        err = norm(gθ)

    end

    @pack m = θ

    return m
end

########################################################################
#################### Simulate the Model ################################
########################################################################


"""
```
History([u, v, θ, f, p, w, i, n])
```
Constructs a History object that records the simulation history for a
run of the Shimer2005 model.  
"""
mutable struct History{T}
    u::Vector{T}
    v::Vector{T}
    θ::Vector{T}
    f::Vector{T}
    p::Vector{T}
    w::Vector{T}
    i::Vector{Int}
    n::Int
end

History() = History(Float64[], Float64[], Float64[], Float64[],
                    Float64[], Float64[], Int[], 0)


"""
```
record!(hist::History, m::Shimer2005, u, h)
```
For a given unemployment rate `u` and latent state index `h`, computes
the vacancy rate `v`, the job finding rate `f`, the aggregate
productivity `p`, the true latent state `y`, the prevailing wage
`w`, and then records them as an additional record in `hist`.  
"""
function record!(hist::History, m::Shimer2005, u, h)
    @unpack θ, β, z, pgrid, ygrid, c = m
    
    # Calculate all the values for that period
    v    = u*θ[h]
    fval = f(m, θ[h])
    p    = pgrid[h]
    y    = ygrid[h]
    w    = (1-β[h])*z + β[h]*(p + c*θ[h])

    record!(hist, u, v, θ[h], fval, p, w, h)
end


"""
```
record!(h::History{T}, u::T, v::T, θ::T, f::T, p::T, w::T,
        i::Int) where T
```
Appends a full observation to the simulation record
"""
function record!(h::History{T}, u::T, v::T, θ::T, f::T, p::T, w::T,
                 i::Int) where T
    push!(h.u, u)
    push!(h.v, v)
    push!(h.θ, θ)
    push!(h.f, f)
    push!(h.p, p)
    push!(h.w, w)
    push!(h.i, i)
    h.n += 1
    return h
end


"""
Constructs a dataframe from the simulation record
"""
function df(h::History)
    t = collect(1:h.n)
    return DataFrame(t=t, u=h.u, v=h.v, θ=h.θ, f=h.f, p=h.p, w=h.w, i=h.i)
end

function Base.show(io::IO, h::History)
    print(io, df(h))
end

function Base.getindex(h::History, idx)
    u = h.u[idx]
    v = h.v[idx]
    θ = h.θ[idx]
    f = h.f[idx]
    p = h.p[idx]
    w = h.w[idx]
    i = h.i[idx]
    n = length(u)
    return History(u,v,θ,f,p,w,i,n)
end

Base.endof(h::History) = h.n
Base.cov(h::History, args...) = Array(h, args...) |> cov
Base.cor(h::History, args...) = Array(h, args...) |> cor 
acor(h::History, args...) = begin
    X       = Array(h, args...)
    n, m    = size(X)
    out     = zeros(m)
    for i=1:m
        out[i] = cor(X[2:end,i], X[1:end-1,i])
    end
    return out
end

@generated function Base.Array(h::History, 
   names::NTuple{N, Symbol}=(:u, :v, :θ, :f, :p)) where N
    
    ex = :(hcat(getfield(h, names[1])))
    for i=2:N
        push!(ex.args, :(getfield(h, names[$i])))
    end
    return ex
end

function Base.map(f::Function, h::History, names=(:u, :v, :θ, :f, :p))
    new = deepcopy(h)
    for name in names
        setfield!(new, name, f(getfield(new, name)))
    end
    return new
end

detrend(h::History, λ, args...) = map(x->x-hpfilter(x, λ), h, args...)

"""
```
step_u(u, θ, Δt)
```
Step the unemployment rate forward in time by Δt when the current
unemployment is u and the current market tightness is θ
"""
function step_u(m::Shimer2005, u, θ, Δt)
    @unpack s = m
    A = (s/(f(m,θ) + s) - u)
    B = 1 - exp(-Δt*(f(m,θ) + s))
    return u + A*B
end

function step!(hist::History, m::Shimer2005, u, h, Δt=-log(rand())/λ)
    
    @unpack θ, ygrid, λ, n, Δ = m

    elapsed = 0.0

    # Check whether it's time to sample the economy again
    while elapsed + Δt < 1

        # Step the diffeq for u forward to the next arrival time
        u       =  step_u(m, u, θ[h], Δt)

        # Update the elapsed time counter
        elapsed += Δt

        # Our poisson process for productivity arrives
        if rand() < 1/2*(1-ygrid[h]/(n*Δ))
            h += 1
        else
            h += -1
        end

        # Generate a new arrival time
        Δt      =  -log(rand())/λ
    end

    # Step the economy forward to the moment we record the sample
    u    = step_u(m, u, θ[h], 1 - elapsed)

    # Record the sample
    record!(hist, m, u, h)

    # Fix Δt to account for the progress we've made
    Δt      = Δt - (1-elapsed)

    return u, h, Δt

end

function simulate!(hist::History, m::Shimer2005; T=1212)
    
    @unpack n, s, θ, λ = m

    # Initialize the productivity at zero
    if hist.n > 0
        h = hist.i[end]
        u = hist.u[end]
    else
        # Start at the steady state
        h  = floor(Int, (n-1)/2)
        u  = s/(s + f(m,θ[h]))
    end

    # Compute the first waiting time
    Δt      = -log(rand())/λ

    # Step the simulation forward T periods
    for t=1:T
        u, h, Δt = step!(hist, m, u, h, Δt)
    end

    return hist
end

function simulate(θ; kwargs...)

    hist = History()
    return simulate!(hist, θ; kwargs...)
end

########################################################################
#################### Pretty Output #####################################
########################################################################

function output_table(labels, stds, stds_std, acors, acors_std, cors,
                      cors_std; fmt="{:.3f}")

    # Size Checking
    length(labels)==length(stds)==length(acors)==size(cors,2) || begin
        error("Size Mismatch")
    end

    t = Table()
    for i=1:length(labels)
        col = TableCol(labels[i], [],[], fmt=fmt)
        sd_lab = "Standard Deviation"
        ac_lab = rpad("Autocorrelation", length(sd_lab))
        col[sd_lab] = (stds[i], stds_std[i])
        col[ac_lab] = (acors[i], acors_std[i])
        for j=1:i

            lab = "Correlation:"
            tab = " "^(length(sd_lab) - length(lab) - length(labels[j]))
            lab = lab*tab*labels[j]
            col[lab] = (cors[i,j], cors_std[i,j])
        end
        push!(t, col)
    end
    return t
end


########################################################################
#################### Run the Code ######################################
########################################################################


function run(m::Shimer2005, fpath::AbstractString; 
             labels=(:u, :v, :θ, :f, :p))

    # Solve the model
    θ = solve!(m)

    # Initialize the model
    hist = simulate(m; T=1000)

    # Simulate the model many many times
    n = length(labels)
    N = 10000
    stds  = zeros(n, N)
    acors = zeros(n, N)
    cors  = zeros(n,n,N)
    regs  = zeros(2, N)
    
    # Get labels for sub
    slabel = union(labels, (:w,:p)) |> Tuple 
    
    # Run the simulations
    for i=1:N
        hist = simulate!(hist, m, T=212)
        sub  = map(hist[end-211:end], slabel) do x
            return log.(x) - hpfilter(log.(x), 1e5)
        end

        # Compute Autocorrelations
        acors[:,i] = acor(sub, labels)

        # Compute Standard Deviations
        stds[:, i] =  Array(sub, labels) |> x->std(x, 1)

        # Compute Correlation Matrix
        cors[:,:,i] =  cor(sub, labels)

        # Regress log wages on log productivity (hp filtered)
        y = sub.w
        X = hcat(ones(212), sub.p)
        regs[:,i] = X'X\X'y

    end

    # Reduce over the simulations
    cor_mean = mean(cors, 3)
    cor_std  = std(cors,  3)
    acor_mean= mean(acors,2)
    acor_std = std(acors, 2)
    stds_mean= mean(stds, 2)
    stds_std = std(stds,  2)

    # Print the elasticity of wages with respect to productivity
    elast    = mean(regs[2,:])
    elast_sd = std(regs[2,:])
    println("Elastisticity of wages with respect to productivity is "*
            "$elast ($elast_sd)")

    # Make a nice Table
    tlabels = string.(labels) .|> x-> replace(x, "θ", "u/v")
    t       = output_table(tlabels, stds_mean, stds_std, acor_mean,
                           acor_std, cor_mean, cor_std)
    write_tex("output/$(fpath)_table.tex", t)

    # Plot the Beveridge Curve from the most recent simulation
    sim = hist[end-211:end]
    fig, ax = subplots()
    scatter(sim.u, sim.v)
    title("Simulated Beveridge Curve")
    xlabel("Unemployment")
    ylabel("Vacancies")
    # xlim([0.065, 0.0775])
    # ylim([0.05, 0.075])
    savefig("output/$(fpath)_beveridge_curve.pdf")
    close()

    return hist
end


########################################################################
#################### Run the Code ######################################
########################################################################

# Set the seed
srand(1492)

# Baseline Model
m  = Shimer2005()
h1 = run(m, "baseline", labels=(:u, :v, :θ, :f, :p))

# Plot productivity over a long horizon
fig, ax = subplots(2)
p = h1.p[end-2000:end]
ax[1][:plot](log.(p), label="Log Productivity")
ax[1][:plot](hpfilter(log.(p), 1e5), label="Trend")
ax[2][:plot](log.(p) - hpfilter(log.(p), 1e5), label="Detrended")
ax[1][:legend]()
ax[2][:legend]()
fig[:suptitle]("Productivity")
savefig("output/baseline_productivity.pdf")
tight_layout()
close()


# Change Bargaining to be countercyclical -- Note: this uses Shimer's
# Calibration
Φ(x) = cdf(Normal(), x)
Ψ(x) = invlogcdf(Normal(), log(x))

m2   = Shimer2005(r=0.12, s=0.1, μ=1.355, α=0.72, z=0.4, c=0.213,
                  σ=0.099, γ=0.004, pgrid=ones(2001))
m2.β = Φ.(m2.ygrid + Ψ(m2.α))
h2   = run(m2, "bargaining", labels=(:u, :v, :θ, :f, :w))

# Bargaining and Productivity Shocks
m3   = Shimer2005(r=0.12, s=0.1, μ=1.355, α=0.72, z=0.4,
                  σ=0.099, γ=0.004, c=0.213)
m3.β = Φ.(m3.ygrid + Ψ(m3.α))
h3   = run(m3, "bargaining_and_productivity", 
           labels=(:u, :v, :θ, :f, :p, :w))

# Now run a version with a stupidly high z (think Hagedorn and Manovski)
m4   = Shimer2005(z=.95)
h4   = run(m4, "highz", labels=(:u, :v, :θ, :f, :w))

ptab = Table(ptable(m, "Baseline"), 
             ptable(m2, "Bargaining"),
             ptable(m3, "Bargaining + Productivity"),
             ptable(m4, "High z"))
write_tex("output/ptable.tex", ptab)
