\documentclass[header.tex]{subfiles}

\usepackage{dsfont}
\usepackage{fancyhdr}
\pagestyle{fancy}

\def\course{Econ 8581}
\def\name{Jacob Adenbaum}
\lhead{\today}
\chead{\course}
\rhead{\name}
\title{Homework 3: Computing Diamond-Mortensen-Pissarides Models}
\author{\name\\
\course}

\begin{document}
\maketitle 

\section{Introduction}
This homework contains my replication of the Shimer 2005 paper on the
unemployment volatility puzzle.  Computing the Shimer 2005 model
involves two main steps.
\begin{enumerate}[label=\arabic*.]
    \item Solving for the optimal $u/v$ ratio state by state, and
    \item Simulating the model through time conditional on the solution
        to step 1.  
\end{enumerate}

\subsection{Solving for the $u/v$ ratio}

In equilibrium, the ratio of $\theta_{p,s}= u/v$ is pinned down in each
state as the solution to the system of nonlinear equations
\begin{equation}
    {r + s + \lambda \over q(\theta_{p,s})}
    + \beta \theta_{p,s}
    = (1 - \beta) {p - z \over c}
    + \lambda \mathbb E_{p,s}\left[ 1 \over q(\theta_{p',s'} )\right]
    \label{theta}
\end{equation}
Once I have put a grid on the latent state $y$, (which implicitly puts
a linear grid on $p$, $s$, and $\theta$) and identified a markov process
governing the transitions between the states, I can easily solve this
with a simple root finding algorithm (I used a Newton-Raphson method).   

\subsection{Simulating the model}

As in Shimer (2005), I construct a discrete approximation to an
Ornstein Uhlenbeck process.  Let $y$ lie in the grid $\left\{ -n \Delta, \dots, 0,
\dots, n \Delta \right\}$ with $2n+1$ grid points and step size
$\Delta$.  Suppose that whenever a shock hits, $y$ transitions to
$y'$ according to
$$ y' = \begin{cases}
    y + \Delta & \text{ with prob} {1\over 2} \left( 1 - {y \over n
    \Delta }\right) \\
    y - \Delta & \text{ with prob} {1\over 2} \left( 1 + {y \over n
    \Delta }\right) \\
\end{cases}$$
Shimer shows that as the time interval over which we examine this
process becomes small, it approximates the behavior of an
Ornstein-Uhlenbeck process.  I allow the shocks to arrive according to
a Poisson process with intensity $\lambda$. 

Between shocks, $\theta$ is constant.  So, the state variable $u$
evolves according to the backwards-looking differential equation
\begin{equation}
    \dot u(t) = s(t) (1-u(t)) - f(\theta)u(t)
    \label{evolution_u}
\end{equation}
In between shocks, this is solved by
\begin{equation}u(t + \delta t) = 
u(t) + \left[\left( s \over f(\theta) + s \right) - 1 \right]\left( 1 -
\exp\left[ -\delta t (f(\theta) + s) \right]
\right) 
\label{evolution_u_solved}
\end{equation}
for some short time interval $\delta t$.  

In order to step this differential equation forward through time, I
follow the following algorithm:
\begin{enumerate}[label=\arabic*.]
    \item First, I take a random draw for the arrival time of the first
        shock.  If $U_t \sim \text{Uniform}(0,1)$, then I set the first
        arrival time of a shock to be $\delta t = - log(U_0)/\lambda$.
        
    \item Keeping track of how much time has elapsed since I last
        recorded an observation of the process, I now step the
        unemployment state forward to either the arrival of the next
        shock, or the next sampling interval (whichever is sooner)
        according to \eqref{evolution_u_solved}.  
    \item If the next shock arrives at time $t$ before I reach the next
        sampling interval, I resolve the shock, take a new draw $U_t$
        and set the new time to arrival to be $- log(U_t)/\lambda$ and
        repeat this process until I reach the next sampling interval. 
        
    \item Otherwise, I step the unemployment rate forward to the
        sampling time, record the state of the economy, and then update
        the time until the arrival of the next shock.
        
\end{enumerate}
I repeat this process until I have the desired number of simulated
observations.  

\section{Results}

In this section, I present the results from our replication.  

\begin{table}
    \centering
    \input{output/ptable.tex}
    \caption{Our parameter values in each calibration}
    \label{tab:ptable}
\end{table}

\subsection{Baseline Calibration}
In the baseline calibration of the model, I follow Shimer to set the
model parameters.  I report the parameters that I use for each calibration
in Table \ref{tab:ptable}.  First, I replicate the simulated moments
that Shimer reports in his Table 3: by simulating the economy of 212
periods 10,000 times, I construct Monte Carlo estimates of the variance
of all the economy's variables, as well as their autocorrelations and
correlations with each other.  I report the results in Table
\ref{tab:baseline}.  Encouragingly, I am able to replicate his results
almost exactly.  

\begin{table}
    \centering
    \input{output/baseline_table.tex}
    \caption{Labor Productivity Shocks}
    \label{tab:baseline}
\end{table}

I also plot the Beveridge Curve for one of my simulated economies in
Figure \ref{fig:beveridge_baseline}.  Note that this changes
substantially depending on which exact simulation is used because the
underlying productivity process in this economy exhibits extremely low
frequency fluctuations.  This will not affect the correlations computed
above since they are all run on logged and detrended variables (using an
Hodrick-Prescott Filter).  However, the underlying productivity shocks
exhibit {\it much} higher volatility and persistence than what we
observe in the US data, as we can see in Figure \ref{fig:productivity}.
Over any given 212 quarter sample in the data, productivity will be
driven by a substantial underlying trend.  

I also ran, for each simulated sample, a regression of log detrended
wages on log detrended productivity.  I found an elasticity of wages
with respect to productivity of 0.995 (with standard error 0.0013).  In
this economy, wages move almost one for one with the underlying
productivity shock.  

\begin{figure}[h]
    \centering
    \includegraphics[width=.75\textwidth]{output/baseline_beveridge_curve.pdf}
    \caption{Beveridge Curve for an economy with Productivity shocks}
    \label{fig:beveridge_baseline}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=.75\textwidth]{output/baseline_productivity.pdf}
    \caption{2000 quarters of simulated productivity data}
    \label{fig:productivity}
\end{figure}

\subsection{Bargaining Shocks}

Following Shimer, we allow for ``countercyclical'' bargaining shocks.
That is, we make $\beta$ a declining function of the latent state $y$.
Shimer uses 
$$\beta(y) = \Phi(y + \Phi^{-1}(\alpha)$$
where $\Phi$ is the cumulative distribution function for the standard
normal distribution.  We replicate his results and report them in Table
\ref{tab:bargaining}.  Here, I was not able to replicate his results
quite as cleanly, however the results are quite close. Again, I use the
same parameters as Shimer 2005 and I report them in Table
\ref{tab:ptable}.  

\begin{table}
    \centering
    \input{output/bargaining_table.tex}
    \caption{Bargaining Shocks}
    \label{tab:bargaining}
\end{table}

\subsection{Bargaining and Productivity Shocks}
Now, let's try to put them all together, including both productivity and
bargaining shocks in the same run.  The results from this simulation are
reported in Table \ref{tab:bargaining_productivity}.  Interestingly,
combining both of the shocks actually decreases the volatility of
unemployment, vacancies, and market tightness, although the rest of the
model seems to fit tolerably well (in terms of their cross correlations)

\begin{table}
    \centering
    \input{output/bargaining_and_productivity_table.tex}
    \caption{Bargaining and Productivity Shocks}
    \label{tab:bargaining_productivity}
\end{table}

\subsection{High Replacement Rate}

Last, we increase the replacement rate to $z = 0.95$ so that it is very
close to the average productivity of workers in the economy (and I reset
the bargaining process back to be a constant).  In short, this doesn't
do much in the Shimer Calibration, and this is because there are such
large fluctuations (over long time horizons) of the aggregate
productivity. Over any individual sample, productivity doesn't fluctuate
much relative to it's underlying trend, so while shifting $z$ around
seems to change the level of unemployment, it doesn't much shift the
volatility of log detrended unemployment within any one sample.
Moreover, the process we use to set $p$ as a function of the underlying
latent state explicitly depends on the value of $z$: increasing $z$
drives up the average productivity level, and dampens it's fluctuations
about the long run average. I don't think that  
this is a general result.  I suspect that this is due to
the particular specification of the aggregate productivity shocks\ldots.  
I report the results in \ref{tab:highz}.   

\begin{table}
    \centering
    \input{output/highz_table.tex}
    \caption{High Replacement Rate}
    \label{tab:highz}
\end{table}


\end{document}
