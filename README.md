# Shimer 2005 Replication
This repository contains my replication files and writeup for a
replication of the Shimer 2005 paper on the unemployment volatility
puzzle.  The results are contained in `pset3.pdf`, and the code in
`Shimer2005.jl`.  
